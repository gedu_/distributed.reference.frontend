﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Distributed.Common;
using Serilog;
using System.IO;

namespace Distributed.Reference.Frontend
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }
        public ILoggerFactory LoggerFactory;
        
        public Startup(IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("settings.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .Enrich.FromLogContext()
                .WriteTo.RollingFile(Path.Combine(env.ContentRootPath, Configuration.GetSection("LogFile").Value))
                .CreateLogger();
            
            LoggerFactory = loggerFactory;
        }

        

        public void ConfigureServices(IServiceCollection services)
        {
            var logger = LoggerFactory.CreateLogger("Base logger");

            services.AddMvc();
            services.WithConfig(Configuration);
            services.WithLoggerFactory(LoggerFactory);
            services.RegisterServiceDependencies(false);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddSerilog();
            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
