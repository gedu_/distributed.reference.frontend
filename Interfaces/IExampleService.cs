using System.ServiceModel;
using System.Threading.Tasks;
using Distributed.Common.Attributes;

namespace Distributed.Reference.Backend.Interfaces
{
    [ServiceContract]
    public interface IExampleService {
        [OperationContract()]
        [ServiceTimeout(1000)]
        string Greet();

        [OperationContract(Name = "GreetWithName")]
        [ServiceTimeout(1000)]
        string Greet(string name);
        
        [OperationContract]
        [ServiceTimeout(1000)]
        decimal Add(decimal first, decimal second);

        [OperationContract(IsOneWay = true)]
        Task DoSomething();
    }
}