﻿using System.Threading.Tasks;
using Distributed.Common.Interfaces;
using Distributed.Reference.Backend.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Distributed.Reference.Frontend.Controllers
{
    public class HomeController : Controller
    {
        private IExampleService _exampleService;
        private IPubSubMessenger _pubsubMessenger;
        private IMessengerClient _messenger;
        private ILogger _logger;

        public HomeController(IExampleService exampleService, IPubSubMessenger pubsubMessenger, IMessengerClient messenger, ILogger logger)
        {
            _exampleService = exampleService;
            _pubsubMessenger = pubsubMessenger;
            _messenger = messenger;
            _logger = logger;
        }

        public async Task<IActionResult> Index()
        {
            await _pubsubMessenger.PublishAsync("TEST", "test message !");
            await _pubsubMessenger.PublishAsync("TEST2", "test message2 !");
            await _exampleService.DoSomething();
            
            return Ok(_exampleService.Greet());
        }
        
        [RouteAttribute("greet/{name}")]
        public IActionResult Greet(string name)
        {
            return Ok(_exampleService.Greet(name));
        }
        
        [RouteAttribute("add/{first:int}/{second:int}")]
        public IActionResult Add(int first, int second)
        {
            var result = _exampleService.Add(first, second);
            return Ok(result);
        }
    }
}
